﻿using System;

namespace EPS.Entities
{
    public class Inscripcion : Entidad
    {
        public DateTime Fecha { get; set; }
        public int AlumnoId { get; set; }
        public Alumno Alumno { get; set; }
        public int CursoId { get; set; }
        public Curso Curso { get; set; }
        public int InscripcionTipoId { get; set; }
        public InscripcionTipo InscripcionTipo { get; set; }
    }
}