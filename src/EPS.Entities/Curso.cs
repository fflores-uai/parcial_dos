﻿using System;
using System.Collections.Generic;

namespace EPS.Entities
{
    public class Curso : Entidad
    {

        public Curso()
        {
            this.Inscripciones = new List<Inscripcion>();
        }

        public string Nombre { get; set; }
        public decimal Costo { get; set; }
        public int Cupo { get; set; }
        public DateTime FechaInicio { get; set; }
        public DateTime FechaLimite { get; set; }

        public List<Inscripcion> Inscripciones { get; set; }
    }
}