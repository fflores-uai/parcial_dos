﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EPS.Entities.Dtos
{
    public class CuposPorCurso
    {
        public int CuposTotales { get; set; }
        public int Inscriptos { get; set; }
    }
}
