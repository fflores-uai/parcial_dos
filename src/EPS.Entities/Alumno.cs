﻿namespace EPS.Entities
{
    public class Alumno : Entidad
    {
        public string Nombre { get; set; }
        public string Apellido { get; set; }
        public string Documento { get; set; }
    }
}