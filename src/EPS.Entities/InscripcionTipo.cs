﻿namespace EPS.Entities
{
    public class InscripcionTipo : Entidad
    {
        public string Descripcion { get; set; }
        public decimal Procentaje { get; set; }
    }
}