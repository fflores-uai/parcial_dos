﻿using EPS.Data;
using EPS.Entities;
using System;

namespace EPS.Core
{
    public class InscripcionesManager
    {
        private InscripcionRepository repository;

        public InscripcionesManager()
        {
            this.repository = new InscripcionRepository();
        }

        public bool ValidarCupo(int cursoId)
        {
            var cupos = this.repository.CupoPorCurso(cursoId);
            return cupos.Inscriptos <= cupos.CuposTotales;
        }

        public void Save(int cursoId, int alumnoId, int becaId)
        {
            this.repository.Save(new Inscripcion() { AlumnoId = alumnoId, CursoId = cursoId, InscripcionTipoId = becaId });
        }
    }
}