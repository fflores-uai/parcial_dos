﻿using EPS.Data;
using EPS.Entities;
using System.Collections.Generic;

namespace EPS.Core
{
    public class AlumnosManager
    {
        private readonly AlumnoRepository respository;

        public AlumnosManager()
        {
            this.respository = new AlumnoRepository();
        }

        public void Guardar(Alumno alumno)
        {
            this.respository.Save(alumno);
        }

        public List<Alumno> GetAll()
        {
            return this.respository.GetAll();
        }
    }
}