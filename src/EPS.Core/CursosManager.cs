﻿using EPS.Data;
using EPS.Entities;
using System;
using System.Collections.Generic;

namespace EPS.Core
{
    public class CursosManager
    {
        private CursoRepository repository;

        public CursosManager()
        {
            this.repository = new CursoRepository();
        }

        public Curso GetCursoPorId(int id)
        {
            return this.repository.Find(id);
        }

        public void Modificar(Curso curso)
        {
            this.repository.Update(curso);
        }

        public void Guardar(Curso curso)
        {
            this.repository.Save(curso);
        }

        public List<Curso> GetAll()
        {
            return this.repository.FindAll();
        }
    }
}