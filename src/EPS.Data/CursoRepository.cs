﻿using EPS.Data.Helpers.Extensions;
using EPS.Entities;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;

namespace EPS.Data
{
    public class CursoRepository
    {
        private Conexion conexion;

        public CursoRepository()
        {
            this.conexion = new Conexion();
        }

        public int Save(Curso curso)
        {
            return this.conexion.WriteStore("curso_save", new Hashtable() {
                { "@Costo", curso.Costo},
                { "@Cupo", curso.Cupo},
                { "@FechaInicio", curso.FechaInicio},
                { "@FechaLimite", curso.FechaLimite},
                { "@Nombre", curso.Nombre},
            });
        }

        public Curso Find(int id)
        {
            var source = this.conexion.Read("curso_find", new Hashtable() { { "@id", id } });
            var data = source.Tables[0].Rows;
            var inscriptos = source.Tables[1].Rows;

            var inscriptosList = new List<Inscripcion>();

            if (inscriptos != null)
            {
                foreach (DataRow r in inscriptos)
                {
                    var iTipo = r["InscripcionTipoId"].ToInt();
                    inscriptosList.Add(new Inscripcion()
                    {
                        Id = r["Id"].ToInt(),
                        AlumnoId = r["AlumnoId"].ToInt(),
                        CursoId = r["CursoId"].ToInt(),
                        Fecha = r["Fecha"].ToDatetime(),
                        InscripcionTipoId = iTipo,
                        InscripcionTipo = new InscripcionTipo() { Id = iTipo, Descripcion = r["Descripcion"].ToString(), Procentaje = r["Porcentaje"].ToDecimal() }
                    });
                }
            }

            if (data != null)
            {
                var r = data[0];
                return new Curso()
                {
                    Id = r["Id"].ToInt(),
                    Costo = r["Costo"].ToDecimal(),
                    Cupo = r["Cupo"].ToInt(),
                    FechaInicio = r["FechaInicio"].ToDatetime(),
                    FechaLimite = r["FechaLimite"].ToDatetime(),
                    Nombre = r["Nombre"].ToString(),
                    Inscripciones = inscriptosList
                };
            }
            else
            {
                return null;
            }
        }

        public int Update(Curso curso)
        {
            return this.conexion.WriteStore("curso_update", new Hashtable() {
                { "@Id", curso.Id},
                { "@Costo", curso.Costo},
                { "@Cupo", curso.Cupo},
                { "@FechaInicio", curso.FechaInicio},
                { "@FechaLimite", curso.FechaLimite},
                { "@Nombre", curso.Nombre},
            });
        }

        public List<Curso> FindAll()
        {
            var source = this.conexion.Read("curso_all");
            var cursos = source.Tables[0].Rows;
            var inscriptos = source.Tables[1].Rows;

            var list = new List<Curso>();
            var inscriptosList = new List<Inscripcion>();

            if (inscriptos != null)
            {
                foreach (DataRow r in inscriptos)
                {
                    var iTipo = r["InscripcionTipoId"].ToInt();
                    inscriptosList.Add(new Inscripcion()
                    {
                        Id = r["Id"].ToInt(),
                        AlumnoId = r["AlumnoId"].ToInt(),
                        CursoId = r["CursoId"].ToInt(),
                        Fecha = r["Fecha"].ToDatetime(),
                        InscripcionTipoId = iTipo,
                        InscripcionTipo = new InscripcionTipo() { Id = iTipo, Descripcion = r["Descripcion"].ToString(), Procentaje = r["Porcentaje"].ToDecimal() }
                    });
                }
            }

            if (cursos != null)
            {
                foreach (DataRow r in cursos)
                {
                    var cursoId = r["Id"].ToInt();
                    var ins = inscriptosList.Where(x => x.CursoId == cursoId).ToList();
                    list.Add(new Curso()
                    {
                        Id = cursoId,
                        Costo = r["Costo"].ToDecimal(),
                        Cupo = r["Cupo"].ToInt(),
                        FechaInicio = r["FechaInicio"].ToDatetime(),
                        FechaLimite = r["FechaLimite"].ToDatetime(),
                        Nombre = r["Nombre"].ToString(),
                        Inscripciones = ins != null && ins.Any() ? ins : new List<Inscripcion>()
                    });
                }
            }

            return list;
        }
    }
}