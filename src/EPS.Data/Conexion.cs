﻿using System.Collections;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;

namespace EPS.Data
{
    public class Conexion
    {
        private const string DBNAME = "datacontext";

        public Conexion()
        {
        }

        private void SetParams(SqlCommand command, Hashtable data = null)
        {
            if (data != null)
                foreach (string key in data.Keys) { command.Parameters.AddWithValue(key, data[key]); }
        }

        private string GetConnectionString()
        {
            return ConfigurationManager.ConnectionStrings[DBNAME].ConnectionString;
        }

        public DataSet Read(string query, Hashtable data = null, CommandType type = CommandType.StoredProcedure)
        {
            using (var cnn = new SqlConnection(GetConnectionString()))
            {
                cnn.Open();

                using (var cmd = new SqlCommand(query, cnn))
                {
                    cmd.CommandType = type;

                    SetParams(cmd, data);

                    using (var adt = new SqlDataAdapter(cmd))
                    {
                        using (var ds = new DataSet())
                        {
                            adt.Fill(ds);
                            return ds;
                        }
                    }
                }
            }
        }

        public int WriteQuery(string query, Hashtable data = null)
        {
            return this.Write(query, data, CommandType.Text);
        }

        public int WriteStore(string query, Hashtable data = null)
        {
            return this.Write(query, data, CommandType.StoredProcedure);
        }

        private int Write(string query, Hashtable data = null, CommandType type = CommandType.StoredProcedure)
        {
            using (var cnn = new SqlConnection(GetConnectionString()))
            {
                cnn.Open();

                using (var trn = cnn.BeginTransaction())
                {
                    using (var cmd = new SqlCommand(query, cnn, trn))
                    {
                        cmd.CommandType = type;
                        SetParams(cmd, data);
                        var result = cmd.ExecuteNonQuery();
                        trn.Commit();

                        return result;
                    }
                }
            }
        }
    }
}