﻿using System;

namespace EPS.Data.Helpers.Extensions
{
    public static class DataRowExtensions
    {
        public static bool ToBool(this object row)
        {
            return Convert.ToBoolean(row);
        }

        public static int ToInt(this object row)
        {
            return Convert.ToInt32(row);
        }

        public static double ToDouble(this object row)
        {
            return Convert.ToDouble(row);
        }

        public static DateTime ToDatetime(this object row)
        {
            return Convert.ToDateTime(row);
        }

        public static decimal ToDecimal(this object row)
        {
            return Convert.ToDecimal(row);
        }
    }
}