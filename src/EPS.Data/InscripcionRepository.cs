﻿using EPS.Data.Helpers.Extensions;
using EPS.Entities;
using EPS.Entities.Dtos;
using System.Collections;

namespace EPS.Data
{
    public class InscripcionRepository
    {
        private readonly Conexion conexion;

        public InscripcionRepository()
        {
            this.conexion = new Conexion();
        }

        public CuposPorCurso CupoPorCurso(int cursoId)
        {
            var source = this.conexion.Read("curso_cupos", new Hashtable() { { "@CursoId", cursoId } });
            var data = source.Tables[0].Rows;

            if (data != null)
            {
                var r = data[0];

                return new CuposPorCurso()
                {
                    CuposTotales = r["CuposTotales"].ToInt(),
                    Inscriptos = r["Inscriptos"].ToInt()
                };
            }

            return null;
        }

        public void Save(Inscripcion inscripcion)
        {
            this.conexion.WriteStore("inscripcion_save", new Hashtable() {
                { "@CursoId", inscripcion.CursoId},
                { "@AlumnoId", inscripcion.AlumnoId},
                { "@BecaId", inscripcion.InscripcionTipoId }
            });
        }
    }
}