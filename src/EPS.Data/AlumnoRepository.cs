﻿using EPS.Data.Helpers.Extensions;
using EPS.Entities;
using System.Collections.Generic;
using System.Data;

namespace EPS.Data
{
    public class AlumnoRepository
    {
        private readonly Conexion conexion;

        public AlumnoRepository()
        {
            this.conexion = new Conexion();
        }

        public void Save(Alumno alumno)
        {
            this.conexion.WriteStore("alumno_save", new System.Collections.Hashtable()
            {
                { "@Nombre", alumno.Nombre},
                { "@Apellido", alumno.Apellido },
                { "@Documento", alumno.Documento}
            });
        }

        public List<Alumno> GetAll()
        {
            var source = this.conexion.Read("alumno_all");
            var data = source.Tables[0].Rows;
            var list = new List<Alumno>();

            if (data != null)
            {
                foreach (DataRow r in data)
                {
                    list.Add(new Alumno()
                    {
                        Id = r["id"].ToInt(),
                        Apellido = r["Apellido"].ToString(),
                        Documento = r["Documento"].ToString(),
                        Nombre = r["Nombre"].ToString()
                    });
                }
            }

            return list;
        }
    }
}