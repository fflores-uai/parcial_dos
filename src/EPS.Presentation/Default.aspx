﻿<%@ Page Title="Home Page" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="EPS.Presentation._Default" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">

    <div>
        <h2>ESCUELA PARA SABER</h2>
    </div>

    <h4>Acciones Disponibles:</h4>

    <section class="card-container">

        <div class="card">
            <div class="card-body">
                <h5 class="card-title">ALUMNOS</h5>
                <p class="card-text">Alta de alumnos.</p>
            </div>

            <div class="card-body">
                <a href="\Views\AlumnosABM" class="btn btn-primary btn-sm">Nuevo Alumno</a>
            </div>
        </div>

        <div class="card">

            <div class="card-body">
                <h5 class="card-title">CURSOS</h5>
                <p class="card-text">Alta y modificacion de cursos.</p>
            </div>

            <div class="card-body">
                <a href="\Views\CursosLista" class="btn btn-primary btn-sm">Listado</a>
                <a href="\Views\CursosABM" class="btn btn-warning btn-sm">Nuevo Curso</a>
            </div>
        </div>

        <div class="card">

            <div class="card-body">
                <h5 class="card-title">INSCRIPCIONES</h5>
                <p class="card-text">Agregar alumnos a cada curso con becas.</p>
            </div>

            <div class="card-body">
                <a href="\Views\InscripcionesABM" class="btn btn-primary btn-sm">Alta en sistema</a>
            </div>
        </div>

        
    </section>
</asp:Content>