﻿using EPS.Core;
using EPS.Entities;
using System;

namespace EPS.Presentation.Views
{
    public partial class AlumnosABM : System.Web.UI.Page
    {
        private readonly AlumnosManager manager;

        public AlumnosABM()
        {
            this.manager = new AlumnosManager();
        }

        protected void Page_Load(object sender, EventArgs e)
        {
        }

        protected void Guardar(object sender, EventArgs e)
        {
            var alumno = new Alumno()
            {
                Apellido = this.apellido.Value,
                Documento = this.documento.Value,
                Nombre = this.nombre.Value
            };

            this.manager.Guardar(alumno);

            Response.Redirect("/Views/CursosLista.aspx");
        }
    }
}