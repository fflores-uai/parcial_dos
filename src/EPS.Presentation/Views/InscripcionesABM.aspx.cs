﻿using EPS.Core;
using EPS.Presentation.Helpers.Models;
using Newtonsoft.Json;
using System;
using System.Linq;
using System.Web.Services;
using System.Web.UI.WebControls;

namespace EPS.Presentation.Views
{
    public partial class InscripcionesABM : System.Web.UI.Page
    {
        private CursosManager cursosManager;
        private AlumnosManager alumnosManager;

        public InscripcionesABM()
        {
            this.cursosManager = new CursosManager();
            this.alumnosManager = new AlumnosManager();
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            BindAlumnos();
            BindCursos();
        }

        private void BindCursos()
        {
            this.cursos.Items.Insert(0, new ListItem(""));
            var i = 1;
            this.cursosManager.GetAll().ForEach(x =>
            {
                this.cursos.Items.Insert(i, new ListItem(x.Nombre, x.Id.ToString()));
                i += 1;
            });
        }

        private void BindAlumnos()
        {
            this.alumnos.Items.Insert(0, new ListItem(""));
            var i = 1;
            this.alumnosManager.GetAll().ForEach(x =>
            {
                this.alumnos.Items.Insert(i, new ListItem($"{x.Apellido} {x.Nombre} - DNI : {x.Documento}", x.Id.ToString()));
                i += 1;
            });
        }

        [WebMethod]
        public static string Guardar(int cursoId, int alumnoId, int becaId)
        {
            var manager = new InscripcionesManager();
            var cursoManager = new CursosManager();

            var curso = cursoManager.GetCursoPorId(cursoId);

            // valido si el curso no esta lleno
            if (!manager.ValidarCupo(cursoId))
            {
                return JsonConvert.SerializeObject(new HttpResponse(400, "El curso no cuenta con cupos disponibles."));
            }

            // valido si esta fuera del limite de inscripcion
            if (curso.FechaLimite.Date < DateTime.Today.Date)
            {
                return JsonConvert.SerializeObject(new HttpResponse(400, $"Esta inscripcion se encuentra fuera del limite. Fecha Limite : {curso.FechaLimite.ToString("yyyy-MM-dd")}"));
            }

            // valido si el alumno ya no fue inscripto en este mismo curso
            if (curso.Inscripciones.Any())
            {
                if (curso.Inscripciones.Where(x => x.AlumnoId == alumnoId).Any())
                {
                    return JsonConvert.SerializeObject(new HttpResponse(400, $"El Alumno ya se encuentra inscripto en este curso."));
                }
            }

            manager.Save(cursoId, alumnoId, becaId);

            return JsonConvert.SerializeObject(new HttpResponse(200, ""));
        }
    }
}