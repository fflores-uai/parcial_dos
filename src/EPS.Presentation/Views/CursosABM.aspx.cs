﻿using EPS.Core;
using EPS.Entities;
using System;
using System.Web.UI;

namespace EPS.Presentation.Views
{
    public partial class CursosABM : System.Web.UI.Page
    {
        private CursosManager manager;

        public CursosABM()
        {
            this.manager = new CursosManager();
            this.Title = "Alta";
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (this.IsPostBack)
            {
                // entra despues de hacer submit
            }
            else
            {
                //LoadData();
                LoadCurso();
            }
        }

        private void LoadCurso()
        {
            string Id = Request.QueryString["Id"];
            if (!string.IsNullOrEmpty(Id))
            {
                Page.Title = "Modificación";

                Curso curso = this.manager.GetCursoPorId(Convert.ToInt32(Id));
                this.id.Value = curso.Id.ToString();
                this.nombre.Value = curso.Nombre;
                this.cupo.Value = curso.Cupo.ToString();
                this.fechaInicio.Value = curso.FechaInicio.ToString("yyyy-MM-dd");
                this.fechaLimite.Value = curso.FechaLimite.ToString("yyyy-MM-dd");
                this.costo.Value = curso.Costo.ToString().Replace(',', '.');
            }
        }

        protected void Guardar(object sender, EventArgs e)
        {
            var curso = new Curso()
            {
                Costo = Convert.ToDecimal(this.costo.Value.Trim().Replace(',', '.')),
                Cupo = Convert.ToInt32(this.cupo.Value),
                FechaInicio = Convert.ToDateTime(this.fechaInicio.Value),
                FechaLimite = Convert.ToDateTime(this.fechaLimite.Value),
                Nombre = this.nombre.Value.ToString()
            };

            if (!string.IsNullOrEmpty(this.id.Value))
            {
                curso.Id = Convert.ToInt32(this.id.Value);
                this.manager.Modificar(curso);
            }
            else
            {
                this.manager.Guardar(curso);
            }

            Response.Redirect("/Views/CursosLista");
        }

       
    }
}