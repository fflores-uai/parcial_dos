﻿<%@ Page Title="Alta" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="CursosABM.aspx.cs" Inherits="EPS.Presentation.Views.CursosABM" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">

    <section class="container">

        <h2><%: Title %> Curso</h2>

        <div class="col">

            <input type="hidden" name="id" value="" id="id" runat="server"/>

            <div class="form-group">
                <label for="email">Nombre :</label>
                <input type="text" required runat="server" class="form-control" id="nombre">
            </div>

            <div class="form-group">
                <label for="email">Cupo :</label>
                <input type="number" required runat="server" class="form-control" step="1" id="cupo" min="1" >
            </div>

            <div class="form-group">
                <label for="email">Arancel :</label>
                <input type="number" required runat="server" step="any" class="form-control" id="costo">
            </div>

            <div class="form-group">
                <label for="email">Fecha Inicio :</label>
                <input type="date" required runat="server" class="form-control" id="fechaInicio">
            </div>

            <div class="form-group">
                <label for="email">Fecha Limite :</label>
                <input type="date" required runat="server" class="form-control" id="fechaLimite">
            </div>

            <div class="form-buttons">
               <%--<button runat="server" class="btn btn-primary btn-small" OnClick="Guardar">Guardar</button>--%>
                <asp:Button Text="Guardar" CssClass="btn btn-primary btn-lg" OnClick="Guardar" runat="server" />
                <a href="/Views/CursosLista" class="btn btn-warning btn-lg">Volver</a>
            </div>
        </div>
    </section>
</asp:Content>