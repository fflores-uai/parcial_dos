﻿using EPS.Core;
using EPS.Entities;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Web.Services;

namespace EPS.Presentation.Views
{
    public partial class CursosLista : System.Web.UI.Page
    {
        public List<Curso> Cursos = new List<Curso>();
        private readonly CursosManager manager;
        public decimal recaudacion = 0;

        public CursosLista()
        {
            this.manager = new CursosManager();
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            LoadCursos();
            CalcularRecaudacion();
        }

        private void CalcularRecaudacion()
        {
            decimal total = 0;

            foreach (var c in this.Cursos)
            {
                //foreach (var inscripto in c.Inscripciones)
                //{
                //    if (inscripto.InscripcionTipo.Procentaje == 1)
                //    {
                //        total += c.Costo;
                //    }
                //    else
                //    {
                //        total += (c.Costo - (c.Costo * inscripto.InscripcionTipo.Procentaje));
                //    }
                //}

                total += Sumar(c.Inscripciones, c.Costo);
            }

            this.recaudacion = total;
        }

        private void LoadCursos()
        {
            this.Cursos = this.manager.GetAll();
        }

        [WebMethod]
        public static string Rendicion(int id)
        {
            var cursoManager = new CursosManager();

            var curso = cursoManager.GetCursoPorId(id);

            var total = Sumar(curso.Inscripciones, curso.Costo);

            return JsonConvert.SerializeObject( new { total, curso.Nombre });
        }

        private static decimal Sumar(List<Inscripcion> inscripciones, decimal costo)
        {
            decimal total = 0;

            foreach (var inscripto in inscripciones)
            {
                if (inscripto.InscripcionTipo.Procentaje == 1)
                {
                    total += costo;
                }
                else
                {
                    total += (costo - (costo * inscripto.InscripcionTipo.Procentaje));
                }
            }

            return total;
        }
    }
}