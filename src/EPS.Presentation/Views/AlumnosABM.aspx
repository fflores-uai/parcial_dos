﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="AlumnosABM.aspx.cs" Inherits="EPS.Presentation.Views.AlumnosABM" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <section class="container">
        <h2>Alta Alumno</h2>


        <div class="col">

            <div class="form-group">
                <label for="email" class="required">Nombre :</label>
                <input type="text" runat="server" class="form-control" required id="nombre">
            </div>

            <div class="form-group">
                <label for="email" class="required">Apellido :</label>
                <input type="text" runat="server" class="form-control" required id="apellido">
            </div>

            <div class="form-group">
                <label for="email" class="required">Documento :</label>
                <input type="number" step="any" runat="server" class="form-control" required id="documento">
            </div>

            <div class="form-buttons">
                <asp:Button Text="Guardar" CssClass="btn btn-primary btn-lg" OnClick="Guardar" runat="server" />
                <a href="/" class="btn btn-warning btn-lg">Volver </a>
            </div>
        </div>
    </section>
</asp:Content>