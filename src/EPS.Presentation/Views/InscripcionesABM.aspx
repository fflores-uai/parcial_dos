﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="InscripcionesABM.aspx.cs" Inherits="EPS.Presentation.Views.InscripcionesABM" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">

    <section >

        <h2>Alta Inscripción</h2>

        <div>

            <div class="alert alert-danger" role="alert" hidden>
                <label class="alert-title">Alerta!</label>
                <br />
                <label class="alert-message" id="alert-text">Mensaje error</label>
            </div>

            <div class="form-group">
                <label for="email" class="required">Cursos :</label>
                <select class="form-control" runat="server" ClientIDMode="Static" id="cursos" required></select>
                <small id="curso-error" hidden style="color: #a94442;"></small>
            </div>


            <div class="form-group">
                <label for="email" class="required">Alumnos :</label>
                <select class="form-control error" ClientIDMode="Static"  runat="server" id="alumnos" required></select>
                <small id="alumno-error" hidden style="color: #a94442;">asdasd</small>
            </div>

            <div class="form-group">
                <label for="email" class="required">Becas :</label>
                <select class="form-control" id="becas" required>
                    <option value="1">Regular</option>
                    <option value="2">Media Beca</option>
                    <option value="3">Beca Completa</option>
                </select>
            </div>

            <div>
                <button class="btn btn-primary btn-lg" type="button" onclick="validateAndSave()">Guardar</button>
                <a href="/Views/CursosLista" class="btn btn-info btn-lg" >Volver</a>
            </div>
        </div>
    </section>


    <script>
        function validateAndSave() {

            var isValid = true;

            var data = {
                cursoId: $('#cursos').val(),
                alumnoId: $('#alumnos').val(),
                becaId: $('#becas').val()
            }

            if (data.cursoId == '') {
                $('#curso-error').html("Debe seleccionar un curso").show();
                isValid = false;
            } else { $('#curso-error').hide(); }

            if (data.alumnoId == '') {
                $('#alumno-error').html("Debe seleccionar un alumno").show();
                isValid = false;
            } else { $('#alumno-error').hide(); }

            var data = JSON.stringify(data);

            if (isValid) {

                $.ajax({
                    type: "POST",
                    dataType: "json",
                    url: "InscripcionesABM.aspx/Guardar",
                    contentType: "application/json; charset=utf-8",
                    data: data,
                    success: function (res) {
                        var response = JSON.parse(res.d);

                        console.log(response);

                        if (response.StatusCode == 200) {
                            console.log("great");
                            location.href = "/Views/CursosLista"
                        } else {
                            $('.alert-message').html(response.Message);
                            $(".alert").show();
                        }


                    },
                    error: function (error) {
                        console.log(error);
                    }

                })


            }
            



        }

        

    </script>
</asp:Content>