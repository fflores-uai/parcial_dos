﻿<%@ Page Title="CURSOS" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="CursosLista.aspx.cs" Inherits="EPS.Presentation.Views.CursosLista" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">

    <section class="container">
        <h2>Cursos</h2>

        <br />
        <h4>Recaudacion Total : <%: recaudacion  %></h4>
        <br />

        <input id="idRedirect" ClientIDMode="Static" type="hidden" runat="server" />

        <table class="table">
            <thead>
                <tr>
                    <th>#</th>
                    <th>Nombre</th>
                    <th>Fecha Inicio</th>
                    <th>Fecha Limite</th>
                    <th>Costo</th>
                    <th>Cupo</th>
                    <th>Inscriptos</th>
                    <th></th>
                </tr>
            </thead>
            <tbody>
                <% if (Cursos == null || Cursos.Count == 0) {%> 
            <tr><td colspan="7">No hay cursos cargados</td></tr>
            <%} %>

                <% foreach (var item in Cursos.OrderBy(x => x.Nombre))
                    {%>
                <tr>
                    <td><%= item.Id %></td>
                    <td><%= item.Nombre %></td>
                    <td><%= item.FechaInicio.ToString("yyyy-MM-dd") %></td>
                    <td><%= item.FechaLimite.ToString("yyyy-MM-dd") %></td>
                    <td>$ <%= item.Costo %></td>
                    <td><%= item.Cupo %></td>
                    <td><%= item.Inscripciones.Count() %></td>
                    <td>
                        <button class="btn btn-primary" type="button" onclick="mostrarCurso(this)" data-id="<%= item.Id %>">
                            <span class="glyphicon glyphicon-pencil"></span> 
                        </button>

                        <button class="btn btn-info" type="button" onclick="cargarRendicionCurso(this)" data-id="<%= item.Id %>">
                            <span class="glyphicon glyphicon-usd"></span> 
                        </button>
                    </td>
                </tr>

                <%} %>
                
            </tbody>
            <tfoot>
                <tr>
                    <td colspan="8">
                        <a href="/Views/CursosABM" class="btn btn-primary btn-lg">Agregar Curso</a>
                        <a href="/Views/InscripcionesABM" class="btn btn-info btn-lg">Inscribir Alumno</a>
                    </td>
                </tr>
            </tfoot>
        </table>
    </section>

    <div id="recaudacion-modal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Recaudación Curso : <strong id="nombre-curso"></strong></h4>
      </div>
      <div class="modal-body">
        <p>Total recaudado por el curso: <span id="recaudacion-valor"></span></p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
      </div>
    </div>

  </div>
</div>


    <script>
        function mostrarCurso(elem) {
            var id = $(elem).data('id');
            $('#idRedirect').val(id);
            window.location.href = "/Views/CursosABM.aspx?id=" + id;
        }


        function cargarRendicionCurso(elem) {
            var id = $(elem).data('id');

            console.log(id);

            var data = { id: id };

            $.ajax({
                type: "POST",
                dataType: "json",
                url: "CursosLista.aspx/Rendicion",
                contentType: "application/json; charset=utf-8",
                data: JSON.stringify(data),
                success: function (res) {

                    var recaudacion = JSON.parse(res.d);
                    $('#nombre-curso').html(recaudacion.Nombre)
                    $('#recaudacion-valor').html(recaudacion.total);
                    $('#recaudacion-modal').modal("show");


                },
                error: function (error) {
                    console.log(error);
                }

            })

        }

    </script>
</asp:Content>