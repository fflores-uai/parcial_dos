﻿namespace EPS.Presentation.Helpers.Models
{
    public class HttpResponse
    {
        public HttpResponse(int statusCode, string message)
        {
            StatusCode = statusCode;
            Message = message;
        }

        public int StatusCode { get; set; }
        public string Message { get; set; }
    }
}