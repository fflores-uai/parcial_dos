
use master

go
if (exists(select * from sys.databases where name = 'eps_data'))
drop database eps_data

go
create database eps_data

go
use eps_data

go
IF(EXISTS(SELECT object_id FROM sys.tables WHERE name = 'Alumnos'))
	drop table Alumnos

go
CREATE TABLE Alumnos(
	Id int primary key not null identity(1,1),
	Nombre varchar(256),
	Apellido varchar(256),
	Documento varchar(256)
)

go
IF(EXISTS(SELECT object_id FROM sys.tables WHERE name = 'Cursos'))
	drop table Cursos

go
CREATE TABLE Cursos(
	Id int primary key not null identity(1,1),
	Nombre varchar(256),
	Costo decimal,
	Cupo int, 
	FechaInicio datetime,
	FechaLimite datetime
)


go
IF(EXISTS(SELECT object_id FROM sys.tables WHERE name = 'Inscripciones'))
	drop table Inscripciones

go
CREATE TABLE Inscripciones(
	Id int primary key not null identity(1,1),
	AlumnoId int,
	CursoId int,
	Fecha datetime,
	InscripcionTipoId int
)

go
IF(EXISTS(SELECT object_id FROM sys.tables WHERE name = 'InscripcionTipo'))
	drop table InscripcionTipo

go
CREATE TABLE InscripcionTipo(
	Id int primary key not null,
	Descripcion varchar(256),
	Porcentaje decimal(18,2)
)

go
insert into InscripcionTipo(Id, Descripcion, Porcentaje) values
(1, 'Regular', 0.5),
(2, 'Media Beca', 0.5),
(3, 'Beca Completa', 0.8)


delete from InscripcionTipo

------------------------------------------------------------------------------

-- PROCEDURES
------------------------------------------------------------------------------
go
IF(EXISTS(SELECT object_id FROM sys.procedures WHERE name = 'curso_update'))
	drop procedure curso_update

go
create procedure curso_update
@Id int, 
@Costo decimal,
@Cupo int,
@Nombre varchar(256),
@FechaInicio datetime,
@FechaLimite datetime
as
begin
	update Cursos
	set 
		Costo = @Costo,
		Nombre = @Nombre,
		Cupo = @Cupo,
		FechaInicio = @FechaInicio,
		FechaLimite = @FechaLimite
	where Id = @Id
end

------------------------------------------------------------------------------
go
IF(EXISTS(SELECT object_id FROM sys.procedures WHERE name = 'curso_save'))
	drop procedure curso_save

go
create procedure curso_save
@Costo decimal,
@Cupo int,
@Nombre varchar(256),
@FechaInicio datetime,
@FechaLimite datetime
as
begin
	insert into Cursos(Costo,Cupo,FechaInicio,FechaLimite,Nombre)
	values (@Costo,@Cupo,@FechaInicio,@FechaLimite,@Nombre)
end

------------------------------------------------------------------------------
go
IF(EXISTS(SELECT object_id FROM sys.procedures WHERE name = 'curso_find'))
	drop procedure curso_find

go
create procedure curso_find
@Id int
as
begin
	select * from Cursos where Id = @Id

	select i.*, it.Descripcion, it.Porcentaje 
	from Inscripciones i join InscripcionTipo it on i.InscripcionTipoId = it.Id 
	where i.CursoId = @Id
end

------------------------------------------------------------------------------
go
IF(EXISTS(SELECT object_id FROM sys.procedures WHERE name = 'curso_all'))
	drop procedure curso_all

go
create procedure curso_all
as
begin
	select c.* from Cursos c
	
	select i.*, it.Descripcion, it.Porcentaje from Inscripciones i join InscripcionTipo it on i.InscripcionTipoId = it.Id
end
------------------------------------------------------------------------------
go
IF(EXISTS(SELECT object_id FROM sys.procedures WHERE name = 'alumno_save'))
	drop procedure alumno_save

go
create procedure alumno_save
@Nombre varchar(256), @Apellido varchar(256), @Documento varchar(256)
as
begin
	insert into Alumnos(Nombre,Apellido,Documento) values(@Nombre, @Apellido, @Documento)
end

------------------------------------------------------------------------------
go
IF(EXISTS(SELECT object_id FROM sys.procedures WHERE name = 'alumno_all'))
	drop procedure alumno_all

go
create procedure alumno_all
as
begin
	select * from Alumnos
end

------------------------------------------------------------------------------
go
IF(EXISTS(SELECT object_id FROM sys.procedures WHERE name = 'curso_cupos'))
	drop procedure curso_cupos

go
create procedure curso_cupos
@CursoId int
as
begin
	declare @cupo int = (select Cupo from Cursos where Id = @CursoId)
	declare @inscriptos int = (select count(1) from Inscripciones where CursoId = @CursoId)

	declare @result as table(CuposTotales int, Inscriptos int)
	insert into @result(CuposTotales,Inscriptos) values(@cupo, @inscriptos)

	select * from @result

end

------------------------------------------------------------------------------
go
IF(EXISTS(SELECT object_id FROM sys.procedures WHERE name = 'inscripcion_save'))
	drop procedure inscripcion_save

go
create procedure inscripcion_save
@CursoId int,
@AlumnoId int, 
@BecaId int
as
begin
	
	insert into Inscripciones(AlumnoId, CursoId,Fecha,InscripcionTipoId) values(@AlumnoId,@CursoId,GETDATE(),@BecaId)

end

