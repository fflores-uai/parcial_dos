# README #

** Contenido del cuestionario **

Un establecimiento educativo promueve cursos abiertos de diferentes disciplinas, cada curso tiene un costo específico, una fecha límite de inscripción, y un cupo máximo de alumnos que pueden inscribirse.
Los alumnos pueden ser regulares (abonan el total del curso), alumnos con media beca y por último, alumnos con beca al 80%.

Se requiere un sistema desarrollado en ASP.NET y C# que permita:

* Alta y modificación de cursos
* Alta de alumnos
* Alta de inscripciones con la beca correspondiente
* Informar mediante un evento cuando se quiera inscribir un alumno a un curso completo
* Informar mediante un evento cuando se quiera inscribir un alumno fuera de fecha
* Listar los cursos de forma ordenada por nombre
* Consultar la recaudación total y por curso 

**Requisitos:**

* Arquitectura 4 capas
* BD Sql Server
* Orientado a Objetos